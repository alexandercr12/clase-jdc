# Clase JDC
1. Ir a la carpeta test
2. Crear un nuevo archivo que se llame "sunombreTest.js"

`
const app = require('../app');
const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const DomParser = require('dom-parser');


chai.use(chaiHttp);
it("Tiene el nombre de Alexander", function(done)  {
    chai.request(app).get('/')
    .then(function (response) {
        var document = new DomParser().parseFromString(response.text);
        var text = document.getElementById('name').innerHTML;
        expect(text).to.contains('Alexander');
        done();
    }).catch(function (error) {
        console.log(error);
        done(error);
    });
});`

Deben cambiar el it y el contains.
3. En la carpeta views crear un archivo con su nombre y la extensión .pug
extends layout

`block content
  h1= title
  p#name Welcome to aca va su nombre
`

4. En la carpeta routes deben crear otro archivo con su nombre y la extensión js
Cambiando el / por el que habian dejado en el test y en vez de index poner el nombre del artchivo pug.

`var express = require('express');
var router = express.Router();

router.get('/alexander', function(req, res, next) {
  res.render('alexander', {});
});

module.exports = router;
`