var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var davidRouter = require('./routes/david');
var claudiaRouter = require('./routes/claudia');
var fernandoRouter = require('./routes/fernando');
var cirstianRouter = require('./routes/Cristian');
var fredyRouter = require('./routes/Fredy');
var nicolasRouter = require('./routes/Nicolas-routes');
var rafaelRouter = require('./routes/Rafa');
var mariaRouter = require('./routes/lourdes');
var leonardoRouter = require('./routes/leonardo');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/', davidRouter);
app.use('/', claudiaRouter);
app.use('/', fernandoRouter);
app.use('/', cirstianRouter);
app.use('/', fredyRouter);
app.use('/', nicolasRouter);
app.use('/', rafaelRouter);
app.use('/', mariaRouter);
app.use('/', leonardoRouter);

app.use(function(req, res, next) {
  next(createError(404));
});

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
