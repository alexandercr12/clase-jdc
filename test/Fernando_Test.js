const app = require('../app');
const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const DomParser = require('dom-parser');


chai.use(chaiHttp);

it("La pagina se llama Fernando", function(done)  {
    chai.request(app).get('/fernando')
    .then(function (response) {
        var document = new DomParser().parseFromString(response.text);
        var text = document.getElementById('name').innerHTML;
        expect(text).to.contains('Fernando');
        done();
    }).catch(function (error) {
        console.log(error);
        done(error);
    });
});
