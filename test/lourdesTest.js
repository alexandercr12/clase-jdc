const app = require('../app');
const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const DomParser = require('dom-parser');


chai.use(chaiHttp);

it("Esta es una prueba del nombre Maria", function(done)  {
    chai.request(app).get('/Maria')
    .then(function (response) {
        var document = new DomParser().parseFromString(response.text);
        var text = document.getElementById('name').innerHTML;
        expect(text).to.contains('Maria');
        done();
    }).catch(function (error) {
        console.log(error);
        done(error);
    });
});
