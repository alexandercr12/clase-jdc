const app = require('../app');
const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const DomParser = require('dom-parser');


chai.use(chaiHttp);

it("Tiene el nombre de Claudia", function(done)  {
    chai.request(app).get('/')
    chai.request(app).get('/Claudia')
    .then(function (response) {
        var document = new DomParser().parseFromString(response.text);
        var text = document.getElementById('name').innerHTML;
        expect(text).to.contains('Claudia');
        done();
    }).catch(function (error) {
        console.log(error);
        done(error);
    });
});
