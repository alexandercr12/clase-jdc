var express = require('express');
var router = express.Router();

router.get('/leonardo', function(req, res, next) {
  res.render('leonardo', {});
});

module.exports = router;
